<?php
	$chn = 'index';
?>
<?php include "header.php"; ?>

    <h2>日程安排</h2>
    <p><strong>举办时间：</strong> 2013年7月13日（星期六）、2013年7月14日（星期天）</p>
    <p><strong>举办地点：</strong> 中国·杭州 海外海皇冠假日酒店（<a target="_blank" href="http://hangzhou.edushi.com/c294284.html">地图</a>）</p>

    <a href="http://adc.alibabatech.org/carnival/join" target="_blank" id="signup">报名参加</a>

    <h2>2013年7月13日（混合场）</h2>
    <table>
        <thead>
            <tr>
                <th>时间</th>
                <th>主题</th>
                <th>嘉宾</th>
                <th>公司</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>07:30 - 09:00</td>
                <td>
                  签到时间
                </td>
                <td></td>
                <td></td>
            </tr>		
            <tr>
                <td>09:00 - 09:50</td>
                <td>《<span class="tip" man="王磊" where="支付宝" href="intro.php#wanglei" template_title="1、小微金服用户体验验证体系概况
                    2、session&tracker用户行为监控解决方案 3、用户行为动画还原方案 4、数据成果，CASE分享">用户行为监控</span>》
                    (<a href="ppt/wanglei.7z" target="_blank">PPT下载</a>)
                </td>
                <td><a href="intro.php#wanglei">王磊</a></td>
                <td>支付宝</td>
            </tr>
            <tr>
                <td>09:50 - 10:00</td>
                <td>短休</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>10:00 - 10:50</td>
                <td>《<span class="tip" man="张泉" where="淘宝" href="intro.php#zhangquan" template_title="uitest是一个前端自动化测试工具，为前端测试提供一种更简单更方便的测试方法。相比其它测试工具，uitest有很多为前端量身定制的优点：1.  不但支持单元测试，更能进行UI测试和功能测试。2.  完全使用前端熟悉的javascript语言，在真实的浏览器里进行测试。3.  能够进行多浏览器并行测试。4.  录制工具可以帮你更快的完成测试用例的编写">寻找UItest - 适合前端的测试工具</span>》
                    (<a href="ppt/zhangquan.7z" target="_blank">PPT下载</a>)
                </td>
                <td><a href="intro.php#zhangquan">张泉</a></td>
                <td>淘宝</td>
            </tr>
            <tr>
                <td>10:50 - 11:10</td>
                <td>茶歇、抽奖</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>11:00 - 12:00</td>
                <td>《<span class="tip" man="魏子钧" where="自由职业者" href="intro.php#weizijun" template_title="当把『前端』和『游戏』联系起来的时候，人们通常会很自然的想到『HTML5游戏』，而无意识的忽略了『JS是一门脚本语言，脚本语言天生就是写游戏的料』这样一个事实。在这次的分享中，我将结合实例和自己的一些经验，来聊一聊基于JS的『伪HTML5』游戏开发。">JavaScript is a SCRIPT – 聊聊js在游戏开发里的角色</span>》
                    (<a href="ppt/weizijun.7z" target="_blank">PPT下载</a>)
                </td>
                <td><a href="intro.php#weizijun">魏子钧</a></td>
                <td>自由职业者</td>
            </tr>
            <tr>
                <td>12:00 - 14:00</td>
                <td>午间休息</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>14:00 - 14:50</td>
                <td>《<span class="tip" man="王军" where="一淘" href="intro.php#wangjun" template_title="现代浏览器大多已经可以使用HTML、CSS 和 JavaScript 结合浏览器暴露的 API 来开发浏览器的扩展了。在本次分享中，我会由浏览器扩展开发中的一些概念引入，介绍跨浏览器扩展开发框架的使用方法。继而重点讲解一下框架的设计思想以及实现原理等内容。">跨浏览器扩展开发框架</span>》
                    (<a href="ppt/wangjun.7z" target="_blank">PPT下载</a>)
                </td>
                <td><a href="intro.php#wangjun">王军</a></td>
                <td>一淘</td>
            </tr>
            <tr>
                <td>14:50 - 15:10</td>
                <td>茶歇、抽奖</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>15:10 - 16:00</td>
                <td>《<span class="tip" man="单丹" where="阿里巴巴" href="intro.php#dandan" template_title="CMS[Content management system]是可以编辑修改发布页面的系统。PSD是photoshop的一种专用格式。PSD2CMS定义为由photoshop设计稿自动发布页面的一个系统。使用PSD2CMS可以解决掉哪类问题，是如何做到的，它可以带来怎样的效率提升和工作环境的改善，未来这个系统将如何扩展。我们将一起探讨这些问题。">PSD2CMS - 由设计稿自动发布页面的系统</span>》
                    (<a href="ppt/shandan.7z" target="_blank">PPT下载</a>)
                </td>
                <td><a href="intro.php#dandan">单丹</a></td>
                <td>阿里巴巴</td>
            </tr>
            <tr>
                <td>16:00 - 16:10</td>
                <td>短休</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>16:10 - 17:00</td>
                <td>《<span class="tip" man="郭大扬" where="腾讯" href="intro.php#berg" template_title="Wetop是一款使用html5开发桌面app的引擎。该引擎基于chromium，实现完全用js开发桌面程序，支持透明渲染，提供了丰富的本地API。既利用了js快速开发调试的特性，同时又拥有客户端程序的体验。程序的安装打包也非常简单。使用该引擎，我们可以充分利用html5和css3的特性，快速开发各种界面美观，体验酷炫的桌面软件，诸如浏览器，图片浏览器……。同时，还提供了p2p功能，只需简单几个函数，不需懂得网络编程，即可实现两机直接通信。另外，还有一些系统API，诸如开关机等。其他API如文件IO，二进制处理……等。API还在持续扩展中……总之，这里能满足你的任何想象。">Webtop - HTML5 App开发引擎</span>》
                    (<a href="ppt/guodayang.7z" target="_blank">PPT下载</a>)
                </td>
                <td><a href="intro.php#guodayang">郭大扬</a></td>
                <td>腾讯</td>
            </tr>
        </tbody>
    </table>

    <h2>2013年7月14日（跨终端专场）</h2>
    <table>
        <thead>
        <tr>
            <th width="90px">时间</th>
            <th width="330px">主题</th>
            <th width="210px">嘉宾</th>
            <th>公司</th>
        </tr>
        </thead>
        <tbody>
		<tr>
			<td>07:30 - 09:00</td>
			<td>
			  签到时间
			</td>
			<td></td>
			<td></td>
		</tr>			
        <tr>
            <td>09:00 - 09:50</td>
            <td>《<span class="tip" man="徐凯" where="天猫" href="intro.php#xukai" template_title="移动优先喊了一段时间了，现状多是PC页面上不断做“减法”来适配移动设备浏览器；而移动优先最核心的概念便是优先考虑移动设备上的交互体验、专注于产品需求的实质，其实是需要一个做“加法”的前端设计和开发过程；我们在天猫专辑跨终端的开发实践上整理了一些移动优先前端产品的开发经验，分享出来。">移动优先前端产品的探索</span>》
                (<a href="http://luics.github.io/demo/d2/" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#xukai">徐凯</a></td>
            <td>天猫</td>
        </tr>
        <tr>
            <td>09:50 - 10:00</td>
            <td>短休</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>10:00 - 10:50</td>
            <td>《<span class="tip" man="侯昕杰" where="优众" href="intro.php#houxinjie" template_title="基于优众网移动web app多个版本的开发经验，介绍四部分内容：1.我们项目历代版本的架构变迁，从jQuery Mobile到BackboneJS。2.性能优化，包括对    本地存储以及web sql的运用。3.用到的插件的介绍，包括iScroll、模版引擎artTemplate。4.未来web app开发的趋势，模块化以及按需加载。">优众网移动web app开发实践</span>》
                (<a href="ppt/houxinjie.7z" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#houxinjie">侯昕杰</a></td>
            <td>优众</td>
        </tr>
        <tr>
            <td>10:50 - 11:10</td>
            <td>茶歇、抽奖</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>11:10 - 12:00</td>
            <td>《<span class="tip" man="李晶" where="淘宝" href="intro.php#lijing" template_title="Kissy Mobile是一套面向web无线端的一套解决方案，本主题为大家全面介绍kissy mobile。">Kissy Mobile</span>》
                (<a href="ppt/lijing.7z" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#lijing">李晶</a></td>
            <td>淘宝</td>
        </tr>
        <tr>
            <td>12:00 - 14:00</td>
            <td>午间休息</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>14:00 - 14:30</td>
            <td>《<span class="tip" man="肖志勇" where="淘宝" href="intro.php#xiaozhiyong" template_title="几年前，摄像头、麦克、摄像头、各种传感器等是以PC机的辅助设备出现的。随着科技的发展，出现了智能手机、iTV、PAD、google glass、kindle等产品，他们都能以独立的终端形式出现，每种设备都有自己的特点和特长，能满足用户的不同需求，这些设备越来越多的被人接受和普及，很多人往往在同时使用好几种终端，我们如何利用这一点来改变我们的产品思路？">Cross End</span>》
                (<a href="ppt/xiaozhiyong.rar" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#xiaozhiyong">肖志勇</a></td>
            <td>淘宝</td>
        </tr>
        <tr>
            <td>14:30 - 14:40</td>
            <td>短休</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>14:40 - 15:10</td>
            <td>《<span class="tip" man="赖晓思" where="腾讯" href="intro.php#laixiaosi" template_title="《墙来了》是由@腾讯AlloyTeam 开发的一个基于HTML5的在线体感游戏。我们不需要鼠标与键盘，通过肢体的摆动，就能体验穿墙的刺激与快感。这次分享将让大家看到怎样通过技术与创意的结合，开发一个用摄像头玩的体感游戏。">用摄像头玩游戏——基于HTML5开发体感游戏《墙来了》</span>》
                (<a href="http://www.ipresst.com/play/51af41b9a11dca505d00001e" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#laixiaosi">赖晓思</a></td>
            <td>腾讯</td>
        </tr>
        <tr>
            <td>15:10 - 15:30</td>
            <td>茶歇、抽奖</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>15:30 - 16:20</td>
            <td>《<span class="tip" man="童遥" where="百度" href="intro.php#tongyao" template_title="node.js的出现使持续十年的云端一体构想终于飞入了寻常百姓家。在现在的时代背景和完善的技术工具组合下，应该如何设计一个完整的云端一体实时infrastructure？同时，其上层的应用之间又是如何相互配合，能为移动和互联网产业带来什么新的产品启发？本次也将一同探讨。">云端一体的WebApp实时基础设施设计思路</span>》
                (<a href="ppt/tongyao.7z" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#tongyao">童遥</a></td>
            <td>百度</td>
        </tr>
        <tr>
            <td>16:20 - 16:30</td>
            <td>短休</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>16:30 - 17:20</td>
            <td>《<span class="tip" man="吴亮" where="360" href="intro.php#wuliang" template_title="在终端多元化的移动时代，HTML5赋予前端工程师驾驭各种平台的强大能力。移动时代的设备和平台与传统的PC浏览器比较有较大的差异，Web技术和HTML5本身在移动平台上又备受争议，那么前端开发面向终端前进的道路将何去何从？">从前端到终端 - 跨越平台的前端技术</span>》
                (<a href="ppt/wuliang.7z" target="_blank">PPT下载</a>)
            </td>
            <td><a href="intro.php#wuliang">吴亮</a></td>
            <td>360</td>
        </tr>
        </tbody>
    </table>
	<p><b>注意</b>：以上安排可能会根据具体情况进行一些调整。</p>

    <div class="clearfix">
        <div class="f_left mr30">
            <h2>官方微信</h2>
            <p>微信号：d2_2013</p>
            <img src="http://img01.taobaocdn.com/tps/i1/T1WfS3XrNbXXcmgfwv-430-430.jpg" width="300" />
        </div>

        <div class="f_left">
            <h2>新浪微博</h2>
            <p>账号：D2前端技术论坛&nbsp;&nbsp;<a href="http://weibo.com/d2forum">看看去</a> </p>
            <img src="http://img02.taobaocdn.com/tps/i2/T1oha1XrteXXcSqpvS-508-508.png" width="300"/>
        </div>
    </div>
    <p class="strong">关注官方账号，大会信息将通过官方账号同步推送，参与互动还有机会获得奖品哦~</p>

    <h2>主办单位</h2>
    <p><a href="http://www.alibaba.com/"><img src="http://img04.taobaocdn.com/tps/i4/T1r_zhXb0fXXb714Lb-145-60.png" alt="阿里巴巴"></a></p>

    <h2>特别致谢</h2>
    <div class="thanks">
        <a href="http://www.taobao.com" title="淘宝" target="_blank" class="taobao"><img src="http://www.d2forum.org/d2/5/assets/img/logo_taobao.gif" alt="淘宝网"></a>
        <a href="http://www.tmall.com" title="天猫" class="tmall"><img src="http://img01.taobaocdn.com/tps/i1/T1.LjMXX4iXXbab4nm-189-26.png" alt="天猫"></a>
        <a href="https://www.alipay.com" title="支付宝" class="alipay"><img src="http://img04.taobaocdn.com/tps/i4/T1CDC1XrxdXXachn6o-232-45.png" alt="支付宝"></a>
        <a href="http://www.etao.com" title="一淘" class="etao"><img src="http://img02.taobaocdn.com/tps/i2/T1phFoFg4bXXX_uOTs-95-50.png" alt="一淘"></a>

        <a href="http://www.baidu.com" title="百度"><img src="http://www.d2forum.org/d2/4/assets/img/logo-baidu.png" alt="百度"></a>
        <a href="http://www.qq.com" title="腾讯网"><img src="http://www.d2forum.org/d2/5/assets/img/logo-qq.png" alt="腾讯网"></a>
        <a href="http://www.360.cn" title="360"><img src="http://p2.qhimg.com/t011554d7f1d248cbd5.png" alt="360"></a>
        <a href="http://www.ihaveu.com" title="优众"><img src="http://img02.taobaocdn.com/tps/i2/T1ulW1XCVdXXa1v9ju-254-119.jpg" alt="优众" width="140"></a>
    </div>
    <a class="top" href="#top">回到顶部</a>

<?php include "footer.php"; ?>
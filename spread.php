<?php
	$chn = 'spread';
?>
<?php include "header.php"; ?>

<h2>交通指引</h2>
<p>详细地点：杭州海外海宾馆国际会议展览中心•杭州海外海皇冠假日酒店（杭州市上塘路333号）（<a target="_blank" href="http://hangzhou.edushi.com/c294284.html">详细地图</a>）</p>
<p>附近交通：<a href="http://bus.edushi.com/hangzhou/" target="_blank" template_title="杭州公交查询">杭州公交查询</a></p>
<p>公交德胜西路站：82/K82路大站车</p>
<p>公交德胜新村站：46、K209（夜间线）、K206（夜间线）、K33826/K26、K8012/K12（区间）、K78、J1826/K826（停驶）、K591K27</p>
<p>公交石灰坝站：46、K209（夜间线）、K206（夜间线）26/K26、K78,826/K826（停驶）K59182/K8233/K3312/K123/K3</p>
<p class="mt10"><img src="http://img01.taobaocdn.com/tps/i1/T1iy2fXf4wXXaaRFo3-661-412.jpg" alt="D2目的地" class="select-img"></p>

<div class="contactus mt10">
    <p>咨询电话：0571-85022088- 转分机（13022；11491； 10798；31702；30667）</p>
    <p>工作时间：9:00-18:00（周一至周五）</p>
    <p>咨询邮箱：<a href="mailto:developerclub@alibaba-inc.com">developerclub@alibaba-inc.com</a></p>
</div>

<a class="top" href="http://www.d2forum.org/d2/8/#top">回到顶部</a>
<?php include "footer.php"; ?>

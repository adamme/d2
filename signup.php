<?php
	$chn = 'signup';
?>
<?php include "header.php"; ?>
    <h2>活动报名</h2>
    <div class="activity-register">
        <p>第八届 <a title="D2前端技术论坛官网" target="_blank" href="http://www.d2forum.org">D2 前端技术论坛</a>(Designer & Developer)，报名入口如下：</p>
		<p><strong>请点击：</strong> <a href="http://adc.taobao.com/member/login?fromUrl=_carnival_join" target="_blank">http://adc.taobao.com/member/login?fromUrl=_carnival_join</a></p>
		<p><b>注：</b>第八届 <a title="D2前端技术论坛官网" target="_blank" href="http://www.d2forum.org">D2 前端技术论坛</a>(Designer & Developer)将无缝接入到第三届 <a title="ADC&bull;阿里技术嘉年华官网" href="http://adc.taobao.com/" target="_blank">ADC&bull;阿里技术嘉年华</a>，故统一系统报名。</p>
        <h3>如何获知您是否被邀请？</h3>
        <ul>
            <li>所有报名者都会收到邮件告之是否被邀请（嗯。此邮件可能躺在你 的垃圾邮件夹里，敬请留意）；</li>
			<li>报名信息提交后的一周内，组委会将向您反馈报名结果</li>
            <li>如果您获得邀请函，请在2013年7月13日（07:30开始签到）凭手机号，并将作为您进入会场的唯一凭证。</li>
        </ul>
    </div>

    <a class="top" href="#top">回到顶部</a>
<?php include "footer.php"; ?>
<?php
	$chn = 'casual';
?>
<?php include "header.php"; ?>
    <div class="d2-casual">
        <div class="section-data">
            <h2>数据展示</h2>
            <p>在D2开放报名以来，大家对D2的热情高涨，前端技术各场次已经爆满。</p>
            <p>让我们来看看报名参加D2的人员都是来自哪个角落吧~</p>

            <iframe src="d2-map.html" frameborder="0" height="850" width="100%" scrolling="no"></iframe>

            <p>那么大家的“成分”又是如何呢？让我们继续往下看：</p>

            <iframe src="chart.htm" frameborder="0" height="1150" width="100%" scrolling="no"></iframe>


        </div>

        <div class="section-game">
            <h2>游戏体验</h2>
            <div>
                <p>在跨终端专场中，嘉宾赖晓思分享了一款基于HTML5开发的在线体感游戏《墙来了》。</p>
                <p>该游戏采用 HTML5、Javascript、CSS3 等全新 Web 技术。</p>
                <p>只要有一个浏览器和一个摄像头，我们就可以通过身体的摆动，体验穿墙的刺激与快感。</p>
                <img src="http://www.alloyteam.com/wp-content/uploads/2012/11/12-1024x639.png"/>
                <p>赶紧点击链接：<a href="http://WC.AlloyTeam.com/">http://WC.AlloyTeam.com/</a> 来体验一下吧。</p>
            </div>
        </div>
    </div>

    <a class="top" href="#top">回到顶部</a>
<?php include "footer.php"; ?>

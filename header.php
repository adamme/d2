<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
		<title>第八届D2前端技术论坛</title>
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
		<meta name="format-detection" content="telephone=no">	
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="stylesheet" href="assets/main.css">
		<link rel="stylesheet" href="assets/phone.css">
		<link rel="shortcut icon" href="image/d2.icon" type="image/x-icon" />
		<link rel="apple-touch-icon" sizes="57x57" href="http://www.d2forum.org/d2/7/icon2.png">

		<script type="text/javascript" src="http://a.tbcdn.cn/s/kissy/1.3.0/seed-min.js"></script>
		<!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	</head>

<body id="top">

<h1>D2前端技术论坛 - 跨越</h1>

<?php include "nav.php";?>

<div class="container">
	<div class="detail">
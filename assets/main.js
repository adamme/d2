(function() {

	function getDocumentScrollTop(){
	    return Math.max(window.document["documentElement"].scrollTop,window.document.body.scrollTop);
	}

	KISSY.use("dd,overlay,xtemplate",function(S,DD,Overlay,XTemplate){
	//	S.one(".tooltip").css({'top':(document.documentElement.clientHeight - 208 - 60 + getDocumentScrollTop() + 'px'),'left': (document.documentElement.clientWidth - 230)/2 +'px'});

	    var isTouch = false, bodyContent;
	    var dialog = new Overlay.Dialog({
	        headerContent: '议题介绍',
	        mask: true,
	        align: {
	            points: ['cc', 'cc']
	        },
	        draggable: true,
	        aria:true
	    });
			
		var title_template = KISSY.one('#title_intro_template')[0].innerHTML,
			title_mobile_template = KISSY.one('#title_intro_mobile_template')[0].innerHTML;
		
	    KISSY.one('body').on('touchstart',function(ev){
			isTouch = true;
		});

	    KISSY.one('body').on('touchend',function(ev){
			S.one(".tooltip").hide();
		});

	    KISSY.all('.tip').on('click',function(ev){
			var template = '';
			if(isTouch){
				template = title_mobile_template;
				var node = KISSY.one(ev.currentTarget);
				bodyContent = new XTemplate(template).render({name: ev.currentTarget.textContent,content: node.attr('template_title'),man:node.attr('man'),where:node.attr('where'),href:node.attr('href')});
			}else{	
				template = title_template;
				bodyContent = new XTemplate(template).render({name: ev.currentTarget.innerHTML,content: KISSY.one(ev.currentTarget).attr('template_title')});     /*修改name: ev.currentTarget.textContent为name: ev.currentTarget.innerHTML 以兼容IE8及以下版本*/
				dialog.center();
			}
			dialog.set('bodyContent',bodyContent);
	    	dialog.show();

			S.one('.ks-ext-mask').css({'height':S.one('body').height() + '100px','opacity':'0.8', 'z-index':'1'});
	    });
	});
})();
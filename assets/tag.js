/*
 * D2 2013
 * refer to http://www.libertystudio.net/web-design/css-js-3d-tags.html
 * Author: yunxiu.gs <yunxiu.gs@taobao.com>
 */
(function (name, definition) {
  // this is considered "safe":
  var hasDefine = typeof define === 'function';
  var hasExports = typeof module !== 'undefined' && module.exports;

  if (hasDefine) {
    // AMD Module or CMD Module
    define(definition);
  } else if (hasExports) {
    // Node.js Module
    module.exports = definition();
  } else {
    // Assign to common namespaces or simply the global object (window)
    this[name] = definition();
  }
})('Tag', function (require) {
  var D2R = Math.PI / 180;

  var Tooltip = require('./tooltip');

  function Tag(config){
    var self = this;
    //渲染节点
    self.srcNode = $(config.srcNode);
    //数据源
    self.source = config.source || [];
    //3D球半径
    self.radius = config.radius || 150;
    //跟容器宽，半径类似的一个东西？
    self.size = config.size || 250;
    //与转动速度有关，越大越快
    self.tspeed = config.tspeed || 5;
    self.d = config.d || 300;
    //是否分散球形分布
    self.distr = config.distr || true;
    //椭圆程度？，1为正圆
    self.howElliptical = config.howElliptical || 1;
    self.baseFontSize = config.baseFontSize || 12;
    //颜色主题
    self.theme = config.theme || 'theme0';
    self.hoverZIndex = config.hoverZIndex || 1000;
    self.hoverCls = config.hoverCls || 'hover';
    
    self._active = false;
    self.tagList = [];
    self._init();
  }

  Tag.prototype._init = function(){
    var self = this;

    self._initData();
    self._buildTagList();
    self._bindEvent();
  };

  Tag.prototype._initData = function(){
    var self = this;
    self.mouseX = 0;
    self.mouseY = 0;
    self.lastA = 1;
    self.lastB = 1;
  };

  Tag.prototype._buildTagList = function(){
      var self = this,
        data = self.source,
        tmpNode,
        tmp = {},
        i = {},
        tmpList = [];
        tagList = self.tagList,
        count = 0;

      var level = function(num){
        var p = /^(0\.0*)[^0]+/,
          l,
          ret = 1;
        if(p.test(num.toString())){
          l = RegExp.$1.length;
          switch(l){
            case 2:
              ret = 3;
              break;
            case 3: 
              ret = 2;
              break;
            default: 
              ret = 1;
          }
        } 
        return ret;
        
      };
      $.each(data, function(idx, s){
        if(!$.isArray(s)){
          i.text = s;
          i.cnt = 1;
        }else{
          i.text = s[0];
          i.cnt = s[1];
        }
        count += i.cnt;
        tmpNode = $('<a href="#">' + i.text + '</a>')[0];
        tagList.push({
          node: tmpNode,
          data: {text:i.text,cnt:i.cnt} 
        });

      });
      tagList.sort(function(){
        return Math.random() < 0.5 ? 1 : -1;
      });

      $.each(tagList, function(idx, t){
        var m = t.data.cnt/count;
        t.proportion = level(m) + m;//占比
        Tooltip.attach({
            trigger: t.node,
            refer: t.node,
            tooltip: $('<div class="tip">' + t.data.text + '<br/>人数：' + t.data.cnt + '<br/>占比：' + (t.data.cnt/count*100).toFixed(2) + '%</div>'),
            container: self.srcNode
        });
      });
      self.tagList = tagList;

      return tagList;

  };

  Tag.prototype.render = function(){
      var self = this;
      //self._sineCosine( 0,0,0 );
      
      self._positionAll();//按球形分布
      self._bindHoverEvent();
      self._drawColor();

      // self.timer = setInterval(function(){
      //  self._update.call(self);
      // }, 30);
      self._update();
  };

  Tag.prototype._bindHoverEvent = function(){

    var self = this,
        container = self.srcNode;
      container.find('a').bind('mouseenter',function(ev){
        ev.stopPropagation();
        var target = $(ev.target),
          hoverCls = self.hoverCls;
        self._stopTimer();

        target.addClass(hoverCls).siblings('a').removeClass(hoverCls);
        target.data('status', {
          'z-index': target.css('z-index'),
          'font-size': target.css('font-size'),
          'opacity': target.css('opacity')
        });
        $.each(target.siblings('a'),function(idx, i){
          $(i).data('status',{
            'opacity': $(i).css('opacity')
          });
          $(i).css({
            'opacity': 0.3//$(i).data('status').opacity*
          });
        });
        target.css({
          'z-index': self.hoverZIndex,
          //'font-size': parseInt(target.css('font-size'))*1.5 + 'px',
          'opacity': 1
        });
      }).bind('mouseleave', function(ev){
        ev.stopPropagation();
        var target = $(ev.target),
            hoverCls = self.hoverCls;
        target.removeClass(hoverCls);
        target.css(target.data('status'));
        $.each(target.siblings('a'), function(idx, i){
          $(i).css($(i).data('status'));
        });
        self._startTimer();
      });
  };

  Tag.prototype._startTimer = function(){
    var self = this;
    if(!self.timer){
      self.timer = setInterval(function(){
        self._update.call(self);
      }, 30);
    }
  };
    
  Tag.prototype._stopTimer = function(){
    var self = this;
    if(self.timer){
      clearInterval(self.timer);
      self.timer = null;
    }
  };
    
  Tag.prototype._bindEvent = function(){
      var self = this,
        container = self.srcNode;

      container.mouseenter(function(ev){
        self._active = true;
        self._startTimer();
      });

      container.mouseleave(function(ev){
        self._active = false;
      });

      // container.delegate('a', 'mouseenter', {}, function(ev){
      //   var target = $(ev.target),
      //     hoverCls = self.hoverCls;
      //   self._stopTimer();

      //   target.addClass(hoverCls).siblings().removeClass(hoverCls);
      //   target.data('status', {
      //     'z-index': target.css('z-index'),
      //     'font-size': target.css('font-size'),
      //     'opacity': target.css('opacity')
      //   });
      //   $.each(target.siblings(),function(idx, i){
      //     $(i).data('status',{
      //       'opacity': $(i).css('opacity')
      //     });
      //     $(i).css({
      //       'opacity': 0.5//$(i).data('status').opacity*
      //     });
      //   });
      //   target.css({
      //     'z-index': self.hoverZIndex,
      //     'font-size': parseInt(target.css('font-size'))*1.5 + 'px',
      //     'opacity': 1
      //   });
      // }).delegate('a','mouseleave', {}, function(ev){
      //   var target = $(ev.target),
      //       hoverCls = self.hoverCls;
      //   target.removeClass(hoverCls);
      //   target.css(target.data('status'));
      //   $.each(target.siblings(), function(idx, i){
      //     $(i).css($(i).data('status'));
      //   });
      //   self._startTimer();
      // });
      
      container.mousemove(function(ev){
        var r = self.radius;

        self.mouseX = ev.clientX - (container[0].offsetLeft + container[0].offsetWidth/2);//mouse的x坐标
        self.mouseY = ev.clientY - (container[0].offsetTop + container[0].offsetHeight/2);//mouse的y坐标
        
        if(Math.abs(self.mouseX) <= r && Math.abs(self.mouseY) <= r){
          //self._set('_active', false);
          // if(self.timer){
          //  clearInterval(self.timer);
          //  self.timer = null;
          // }
          //self._stopTimer();
        }else{
          //self._startTimer();
          //self._set('_active', true);
          self.mouseX /= 5;
          self.mouseY /= 5;
        }
      });


    };

    Tag.prototype._update = function(){
      var self = this,
        active = self._active,
        tagList = self.tagList,
        size = self.size,
        radius = self.radius,
        tspeed = self.tspeed,
        mouseX = self.mouseX,
        mouseY = self.mouseY,
        lastA = self.lastA,
        lastB = self.lastB,
        d = self.d,
        howElliptical = self.howElliptical;

      var a, b;//diff的衡量？
      
      if(active){
        a = (-Math.min( Math.max( -mouseY, -size ), size ) / radius ) * tspeed;
        b = (Math.min( Math.max( -mouseX, -size ), size ) / radius ) * tspeed;
      }else{
        a = lastA * 0.98;
        b = lastB * 0.98;
      }
      
      self.lastA = a;
      self.lastB = b;
      
      if(Math.abs(a) <= 0.01 && Math.abs(b) <= 0.01){
        return;
      }
      
      var c = 0,
        per,
        sinCos;

      sinCos = self._sineCosine(a,b,c);

      for(var j = 0; j < tagList.length; j++){
        var rx1 = tagList[j].cx;
        var ry1 = tagList[j].cy * sinCos[1] + tagList[j].cz * (-sinCos[0]);
        var rz1 = tagList[j].cy * sinCos[0] + tagList[j].cz * sinCos[1];
        
        var rx2 = rx1 * sinCos[3] + rz1 * sinCos[2];
        var ry2 = ry1;
        var rz2 = rx1 * (-sinCos[2]) + rz1 * sinCos[3];
        
        var rx3 = rx2 * sinCos[5] + ry2 * (-sinCos[4]);
        var ry3 = rx2 * sinCos[4] + ry2 * sinCos[5];
        var rz3 = rz2;
        
        tagList[j].cx = rx3;
        tagList[j].cy = ry3;
        tagList[j].cz = rz3;
        
        per = d/(d + rz3);
        
        tagList[j].x = (howElliptical * rx3 * per) - (howElliptical * 2);
        tagList[j].y = ry3 * per;
        tagList[j].scale = per;
        tagList[j].alpha = per;
        
        tagList[j].alpha = (tagList[j].alpha - 0.6) * (10/6);
      }
      
      self._doPosition();
      self._depthSort();

    };

    Tag.prototype._depthSort = function(){
      var self = this,
        tagList = self.tagList,
        tmp = [],
        i;

      $.each(tagList, function(idx, t){
        tmp.push(t.node);
      });
      
      tmp.sort(function(vItem1, vItem2){
        if(vItem1.cz > vItem2.cz){
          return -1;
        }else if(vItem1.cz < vItem2.cz){
          return 1;
        }else{
          return 0;
        }
      });
      
      for(i = 0; i < tmp.length; i++){
        tmp[i].style.zIndex = i;
      }

    };

    Tag.prototype._positionAll = function(){
      var self = this,
        tagList = self.tagList,
        container = self.srcNode,
        distr = self.distr,
        radius = self.radius;
      
      //随机渲染节点
      var tmp = [];
      $.each(tagList, function(idx, tag){
        tmp.push(tag.node);
      });
      tmp.sort(function(){
        return Math.random() < 0.5 ? 1 : -1;
      });
      $.each(tmp, function(idx, s){
        container.append(s);
      });

      var phi = theta = 0;
      for(var i = 1, max = tagList.length; i < max + 1; i++){
        if(distr){
          //arc cosine
          phi = Math.acos(-1 + (2 * i - 1)/max);//为了随机分布，将与z轴夹角的cos值等差数列
          theta = Math.sqrt(max * Math.PI) * phi;//cz与x轴夹角同样分散开, 这个系数不太懂？
        }else{
          phi = Math.random() * (Math.PI);
          theta = Math.random() * (2 * Math.PI);
        }
        //phi 与z轴夹角， theta xy平面投影与x轴夹角
        //坐标变换
        tagList[i-1].cx = radius * Math.cos(theta) * Math.sin(phi);//以div中心为x,y,z=0的原点
        tagList[i-1].cy = radius * Math.sin(theta) * Math.sin(phi);
        tagList[i-1].cz = radius * Math.cos(phi);

        tagList[i-1].offsetWidth = tagList[i-1].node.offsetWidth;
        tagList[i-1].offsetHeight = tagList[i-1].node.offsetHeight;
        
        tagList[i-1].node.style.left = tagList[i-1].cx + container[0].offsetWidth/2 - tagList[i-1].offsetWidth/2+'px';
        tagList[i-1].node.style.top = tagList[i-1].cy + container[0].offsetHeight/2 - tagList[i-1].offsetHeight/2+'px';
      }


    };

    Tag.prototype._drawColor = function(){
      var self = this,
        tagList = self.tagList,
        i,
        l;

      var tmp = [];
      $.each(tagList, function(idx, tag){
        tmp.push(tag.node);
      });
      tmp.sort(function(){
        return Math.random() < 0.5 ? 1 : -1;
      });
      
      for(i = 0, l = tmp.length; i < l; i++){
        tmp[i].style.color = self._computeColor(i, l);
      }
      // for(i = 0, l = tagList.length; i < l; i++){
      //  tagList[i].node.style.color = self._computeColor(i, l);
      // }

    };

    Tag.prototype._doPosition = function(){
      var self = this,
        container = self.srcNode[0],
        tagList = self.tagList;

      var l = container.offsetWidth/2,
        t = container.offsetHeight/2;

      for(var i = 0; i < tagList.length; i++){
        tagList[i].node.style.left = tagList[i].cx + l - tagList[i].offsetWidth/2 + 'px';
        tagList[i].node.style.top=tagList[i].cy + t - tagList[i].offsetHeight/2 + 'px';
        
        tagList[i].node.style.fontSize = (Math.ceil(12 * tagList[i].scale/2) + 8)*(tagList[i].proportion) + 'px';
        
        tagList[i].node.style.filter = "alpha(opacity=" + 100 * tagList[i].alpha + ")";
        tagList[i].node.style.opacity = tagList[i].alpha;
      }
    };

    Tag.prototype._sineCosine = function( a, b, c){
      var sinA = Math.sin(a * D2R),
        cosA = Math.cos(a * D2R),
        sinB = Math.sin(b * D2R),
        cosB = Math.cos(b * D2R),
        sinC = Math.sin(c * D2R),
        cosC = Math.cos(c * D2R);
      return [sinA, cosA, sinB, cosB, sinC, cosC];
    };

    Tag.prototype._computeColor = function(i, l){
      var self = this,
        theme = self.theme,
        colorArr = self.Themes[theme].COLOR_ARGS,
        size = colorArr.length,
        m,
        color1,
        color2,
        ratio;
      if(l < size){
        //color1 = colorArr[(i+l)%l];
        //color2 = colorArr[(i+1+l)%l];
        color1 = colorArr[i][0];
        color2 = colorArr[i][1];
        ratio = 0;
      }else{
        m = Math.floor(i/l*size);
        color1 = colorArr[m][0];
        color2 = colorArr[m][1];
        //color1 = colorArr[m];
        //color2 = colorArr[(m+1+size)%size];
        ratio = (i - m*Math.ceil(l/size))/Math.ceil(l/size); 
      }
      return interpolateRGB(color1, color2, ratio);
    };

    Tag.prototype.Themes = {
      theme0: {
            COLOR_ARGS: [
                ["#8be62f", "#bfff7f"],
                ["#14cc14", "#66ff66"],
                ["#2fe68a", "#7fffc0"],
                ["#2f8ae7", "#7fc0ff"],
                ["#8a2ee7", "#bf7fff"],
                ["#f33dc6", "#ff8ce3"]
            ]
        },
        theme1: {
            COLOR_ARGS: [
                ["#e72e8b", "#ff7fbf"],
                ["#d94f21", "#ff9673"],
                ["#f3c53c", "#ffe38c"],
                ["#8be62f", "#bfff7f"],
                ["#14cc14", "#66ff66"],
                ["#2fe68a", "#7fffc0"]
            ]
        },
        theme2: {
            COLOR_ARGS: [
                ["#3dc6f4", "#8ce3ff"],
                ["#214fd9", "#7396ff"],
                ["#4f21d9", "#9673ff"],
                ["#c43df2", "#e38cff"],
                ["#d8214f", "#ff7396"],
                ["#f3c53c", "#ffe38c"]
            ]
        },
        theme3: {
            COLOR_ARGS: [
                ["#2f8ae7", "#896DA3"],
                ["#8e34df", "#FFADA6"],
                ["#f738c0", "#65FCFC"],
                ["#84e653", "#555566"],
                ["#0cc53e", "#db3f7c"],
                ["#00e793s", "#db3f7c"]
            ]
        },
        theme4: {
            COLOR_ARGS: [
                ["#d94f21", "#7a88d1"],
                ["#579ce2", "#87bdf4"],
                ["#3bb4df", "#7fd1ef"],
                ["#a380ff", "#baa0ff"],
                ["#a164c5", "#c28fe1"],
                ["#d93a92", "#ec74b6"],
                ["#b82377", "#d569a7"],
                ["#bb3ca3", "#d381c2"],
                ["#da2d57", "#ec6b8a"],
                ["#4ca716", "#4ca716"],
                ["#5b63c2", "#8e93d7"],
                ["#15a9a3", "#4ecac5"],
                ["#a9ab48", "#e8c670"],
                ["#2aa5f5", "#73c4fa"],
                ["#f67e10", "#feb648"],
                ["#1faa77", "#62c8a2"],
                ["#eb4f20", "#f58563"],
                ["#ffc000", "#ffd659"],
                ["#f16ebc", "#f6a1d3"],
                ["#d23457", "#e27b92"]
            ]
        }
    };

  function toColor(a){
    return 16 > a ? "0" + Math.max(0, a).toString(16) : Math.min(255,a).toString(16);
  }

  function toRgb(a){
    var patternRGB = /^#([0-9a-f]{1,2})([0-9a-f]{1,2})([0-9a-f]{1,2})$/,
      r,
      g,
      b;
    if(patternRGB.test(a)){
      r = RegExp.$1.toString();
      g = RegExp.$2.toString();
      b = RegExp.$3.toString();
      return {
        r: parseInt('0x' + (r.length < 2 ? (r+r): r)),
        g: parseInt('0x' + (g.length < 2 ? (g+g): g)),
        b: parseInt('0x' + (b.length < 2 ? (b+b): b))
      };
    }
    return {
      r: 0,
      g: 0,
      b: 0
    };
  }

  function interpolateRGB(c1, c2, ratio){
    var a = toRgb(c1),
      b = toRgb(c2),
      diffR = b.r - a.r,
      diffG = b.g - a.g,
      diffB = b.b - a.b;
      
    return "#" + toColor(Math.round(a.r+diffR*ratio))
          +toColor(Math.round(a.g+diffG*ratio))
          +toColor(Math.round(a.b+diffB*ratio));
  }

  return Tag;
});

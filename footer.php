</div>
</div>
<p class="copyright">®2007-2013 d2forum.org. All rights reserved</p>

<!-- title intro strat -->
<script id="title_intro_template" type="kissy/xtemplate">
    <div class="dialog">
        <dl>
            <dt>《{{name}}》</dt>
            <dd>{{content}}</dd>
        </dl>
    </div>
</script>

<script id="title_intro_mobile_template" type="kissy/xtemplate">
    <div class="dialog">
        <dl>
            <dt>议题：《{{name}}》</dt>
            <dd>议题介绍：{{content}}</dd>
            <dt>嘉宾：<a href="{{href}}">{{man}}</a> ({{where}})</dt>
        </dl>
        <p class="mobile-btn"><button id="J_moblie_close">关闭</button></p>
    </div>
</script>
<!-- title intro end -->

<script type="text/javascript" src="assets/main.js"></script>
</body>
</html>
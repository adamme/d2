<?php
	$chn = 'intro';
?>
<?php include "header.php";?>

    <h2>嘉宾介绍</h2>
    <ul class="speaker-intro">
        <li class="vcard" id="wanglei">
            <img class="photo" src="http://img03.taobaocdn.com/tps/i3/T1ofW3XpFbXXcoPeYv-150-150.png" alt="王磊">
            <h4 class="fn">王磊 <span class="org">支付宝</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/u/1690624767" target="_blank" title="@老鱼ioldfish">http://weibo.com/u/1690624767</a></p>
            <p class="intro">介绍：浸淫前端开发领域近10年，乐此不疲，初心不改。现就职于小微金服担任前端技术专家，致力于用户行为监控体系的建设，同时，就前端创新应用做积极探索，在交互设计、服务端应用研发上也略有研究。</p>
        </li>
        <li class="vcard" id="zhangquan">
            <img class="photo" src="http://img04.taobaocdn.com/tps/i4/T1Vhy0XuhfXXcoPeYv-150-150.png" alt="张泉">
            <h4 class="fn">张泉 <span class="org">淘宝</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/u/1622253490" target="_blank" title="@张泉zZ">http://weibo.com/u/1622253490</a></p>
            <p class="intro">淘宝前端技术专家，目前负责淘宝互动业务前端团队，专注在淘宝SNS化，淘宝SNS业务拓展的前端开发工作，并在前端自动化测试方面有较多的实践和经验。</p>
        </li>
        <li class="vcard" id="weizijun">
            <img class="photo" src="http://img04.taobaocdn.com/tps/i4/T1evNmFh0XXXcoPeYv-150-150.png" width="150" height="150" alt="">
            <h4 class="fn">魏子钧 <span class="org">自由职业者</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/finscn" target="_blank" title="@大城小胖">http://weibo.com/finscn</a></p>
            <p class="url">博客：<a href="http://fins.iteye.com" target="_blank">http://fins.iteye.com</a></p>
            <p class="intro">工作8年, 『折腾HTML5游戏折腾得快挂了』的胖码农。HTML5技术爱好者，曾在HP和盛大做码农，前『前端工程师』，现『做游戏的』。坚持走在『用JS来做游戏』的不归路上。</p>
        </li>
        <li class="vcard" id="wangjun">
            <img class="photo" src="http://img01.taobaocdn.com/tps/i1/T1nre4XrJXXXcoPeYv-150-150.png" alt="王军">
            <h4 class="fn">王军 <span class="org">一淘</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/xingyunshisui" target="_blank" title="@__Meteora__">http://weibo.com/xingyunshisui</a></p>
            <p class="intro">介绍：Web开发者，浏览器扩展开发者，W3Help内容贡献者。现就职于阿里巴巴一淘及搜索事业部，主要负责如意淘浏览器扩展的前端开发。5年Web前端开发经验，2年+的浏览器扩展开发经验，热衷于各类新技术的探索和性能优化。持续关注HTML5/CSS3的发展，近期在Web App开发的道路上摸索着。完美主义倾向，代码洁癖及强迫症重度患者，热爱自由，崇尚黑客精神。
                技术喜好之外，喜欢足球和台球，热爱摇滚，林肯公园死忠。</p>
        </li>
        <li class="vcard" id="dandan">
            <img class="photo" src="http://img04.taobaocdn.com/tps/i4/T1EZW4Xy4XXXcoPeYv-150-150.png" alt="单丹">
            <h4 class="fn">单丹 <span class="org">阿里巴巴</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/sapphireshan" target="_blank" title="@sapphireshan">http://weibo.com/sapphireshan</a></p>
            <p class="url">博客：<a href="http://asasa.tk/" target="_blank">http://asasa.tk/</a></p>
            <p class="intro">介绍：阿里巴巴大采购开发部的一名资深前端工程师。毕业于天津大学，来自于辽宁沈阳。爱好户外运动、旅行、摄影和跳舞。热爱编码，热衷前端技术。应用node、mongodb完成多项应用，也曾致力于前端单元测试。目前专注于改善前端工作环境的工具类产品研发。</p>
        </li>
        <li class="vcard" id="guodayang">
            <img class="photo" src="http://img02.taobaocdn.com/tps/i2/T1LW14XtxXXXcoPeYv-150-150.png" alt="郭大扬">
            <h4 class="fn">郭大扬 <span class="org">腾讯</span></h4>
            <p class="url">博客：<a href="http://www.alloyteam.com" target="_blank">http://www.alloyteam.com</a></p>
            <p class="intro">介绍：未进腾讯前，做过windows程序开发，网站开发，flash小游戏开发。进腾讯前两年从事前端开发，参与过webQQ开发，开发了flassQQ组件。现在从事android手机开发。个人认为语言并不是关键，关键是实现需求。所以如何快速的实现需求，是我们需要考虑的。</p>
        </li>
        <li class="vcard" id="xukai">
            <img class="photo" src="http://img01.taobaocdn.com/tps/i1/T18Ya4XrNXXXa26KYv-150-151.png" alt="徐凯">
            <h4 class="fn">徐凯 <span class="org">天猫</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/777865156" target="_blank" title="@徐凯-鬼道">http://weibo.com/777865156</a></p>
            <p class="url">博客：<a href="http://luics.github.io" target="_blank">http://luics.github.io</a> <a href="http://luics.cnblogs.com" target="_blank">http://luics.cnblogs.com</a></p>
            <p class="intro">介绍：鬼道（徐凯），曾就职百度移动变现团队，所带领的前端团队负责移动广告SDK、移动统计、移动网盟推广、移动网盟；现就职于天猫，负责天猫跨终端的Web业务推进和技术基础设施的建设。目前关注的主要领域是移动和PC Web前端的融合。</p>
        </li>		
        <li class="vcard" id="houxinjie">
            <img class="photo" src="http://img02.taobaocdn.com/tps/i2/T1gZKPXxXgXXcoPeYv-150-150.png" alt="侯昕杰">
            <h4 class="fn">侯昕杰 <span class="org">优众</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/u/1658653440" target="_blank" title="@侯昕杰">http://weibo.com/u/1658653440</a></p>
            <p class="intro">介绍：做个人网站入行，先后从事过php、ruby on rails开发，进入优众网后，最终扎根于前端，尤其熟悉移动web app的开发，精通Backbone.js框架，专注于项目管理，对互联网产品也略有研究。</p>
        </li>
        <li class="vcard" id="lijing">
            <img class="photo" src="http://img03.taobaocdn.com/tps/i3/T1_meOXuFjXXcoPeYv-150-150.png" alt="李晶">
            <h4 class="fn">李晶 <span class="org">淘宝</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/jayli" target="_blank" title="@拔赤">http://weibo.com/jayli</a></p>
            <p class="url">博客：<a href="http://jayli.github.com" target="_blank">http://jayli.github.com</a></p>
            <p class="intro">介绍：李晶，花名拔赤，淘宝前端工程师，具有多年前端开发经验，在团队协作、组件开发、移动Web App等方面有深入研究，曾经参与淘宝首页、KISSY等项目开发。他翻译过《JavaScript Web富应用开发》、《JavaScript权威指南（第六版）》。</p>
        </li>
        <li class="vcard" id="xiaozhiyong">
            <img class="photo" src="http://img02.taobaocdn.com/tps/i2/T1Duq3XrpbXXcoPeYv-150-150.png" alt="肖志勇">
            <h4 class="fn">肖志勇 <span class="org">淘宝</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/satans17" target="_blank" title="@何-畏">http://weibo.com/satans17</a></p>
            <p class="url">博客：<a href="http://www.lzlu.com/" target="_blank">http://www.lzlu.com/</a></p>
            <p class="intro">介绍：毕业后一直从事.net开发，参与开发过不少企业级应用。后来被前端技术能创造的各种神奇效果所吸引，逐渐转做前端，于2010年4月加入淘宝UED。做过淘宝商家，开放平台，目前在淘宝CRM平台开发内部产品。对html5、无线web、跨终端交互在产品的应用有浓厚兴趣。</p>
        </li>
        <li class="vcard" id="laixiaosi">
            <img class="photo" src="http://img02.taobaocdn.com/tps/i2/T1k.doFm8bXXcoPeYv-150-150.png" alt="赖晓思">
            <h4 class="fn">赖晓思 <span class="org">腾讯</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/csonlai" target="_blank" title="@我系__小C">http://weibo.com/csonlai</a></p>
            <p class="url">博客：<a href="http://www.cnblogs.com/Cson/" target="_blank">http://www.cnblogs.com/Cson/</a></p>
            <p class="intro">介绍：Web前端工程师，就职于腾讯，是腾讯AlloyTeam核心成员，热爱HTML5游戏与移动web开发。</p>
        </li>
        <li class="vcard" id="tongyao">
            <img class="photo" src="http://img01.taobaocdn.com/tps/i1/T1hne2XshcXXcoPeYv-150-150.png" alt="童遥">
            <h4 class="fn">童遥 <span class="org">百度</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/imtongyao" target="_blank" title="@童遥_">http://weibo.com/imtongyao</a></p>
            <p class="intro">介绍：专注于跨界APP和云App技术研发，关注云端一体，实时Web等领域话题。坚定认为Javascript将在云端一体和移动实时性应用程序设计中占有重要地位。</p>
        </li>
        <li class="vcard last" id="wuliang">
            <img class="photo" width="150px" src="http://img03.taobaocdn.com/tps/i3/T17Fa3XCRaXXcoPeYv-150-150.png" alt="吴亮">
            <h4 class="fn">吴亮 <span class="org">360</span></h4>
            <p class="twitter">微博：<a href="http://weibo.com/silverna" target="_blank" title="@十年踪迹">http://weibo.com/silverna</a></p>
            <p class="url">博客：<a href="http://www.silverna.org/blog/" target="_blank">http://www.silverna.org/blog/</a></p>
            <p class="intro">介绍：前端开发工程师、技术经理，曾经就职于百度，目前在360负责奇舞团。关注前端以及手机和平板终端应用的开发。</p>
        </li>
    </ul>

    <a class="top" href="#top">回到顶部</a>
<?php include "footer.php"; ?>